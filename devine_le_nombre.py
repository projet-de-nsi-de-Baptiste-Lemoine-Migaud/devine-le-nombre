from random import *

nombremax = 100
essaismax = 10

commencer="oui" #pour commencer la partie il faut mettre "oui"
while commencer=="oui":#Pour commencer a jouer
    atrouver=random() #Prend un nombre aléatoire
    atrouver=int(nombremax*atrouver+1) #pour que le nombre soit entre 1 et 100
    print ("Choisissez un nombre entre 1 et 100 : ")
    essai=0

    while True: # ici un while True car on va faire la sortie de la boucle dans le code
        essai += 1 # on aurait aussi pu metttre : essai = essai + 1		
        entree=input("Nombre = ") #on doit entrer un nombre
        entree=int(entree) #on precise bien que ce nombre doit etre un entier
        
		# Test du nombre entré
        if entree==atrouver: #si on trouve le nombre a deviner en 1 essai
            if essai==1:
                print("Tu es vraiment très fort !!!")
            print("Bravo tu as gagné !")
            break	# sortie de la boucle on a gagné :)
        elif entree < atrouver: #si le nombre proposé est plus petit que le nombre a deviner alors ca nous dis : Trop petit
            print ("Trop petit! Au fait il vous reste ",10-essai,"essais.")
        elif entree > atrouver: #si le nombre proposé est cette fois plus grand que le nombre a deviner alors ca nous dit : Trop grand
            print ("Trop grand! Au fait il vous reste",10-essai,"essais.")
            if entree>nombremax: #Si on donne un nombre plus grand que le nombremax a savoir 100 alors on nous repropose de choisir un nombre sans pour autant nous enlever un essai
                print("Ce nombre est supérieur à 100. Choisissez un nombre entre 1 et 100 : ")
		
        if essai == essaismax: #si notre nombre d'essai est de 5 alors on perd
            print("Dommage...Vous avez perdu car vous n'avez plus d'essais. Le nombre qu'il fallait trouver est :",atrouver)
            break # sortie de la boucle on a perdu :(
   
		# nouvelle partie ?
    commencer=input("Voulez-vous recommencer ? oui/non : ") # si l'on veut recommencer on écrit soit oui ou soit non